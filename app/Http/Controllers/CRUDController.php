<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Midterm;

class CRUDController extends Controller
{
	public function index()
	{
		$midterms=Midterm::get();
		return view('question',compact('midterms'));
	}
    public function create()
    {
        return view('create');
    }
    public function store(Request $request)
    {
        $midterms = new Midterm([
          'question_name' => $request->get('question_name_one'),
          'question' => $request->get('question_one')
        ]);

        $midterms->save();
        return redirect('/midterm');
    }
}
